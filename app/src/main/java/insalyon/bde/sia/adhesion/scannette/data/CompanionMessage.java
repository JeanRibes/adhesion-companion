package insalyon.bde.sia.adhesion.scannette.data;

import com.google.gson.annotations.Expose;

public class CompanionMessage {
    public static final String Type_Status_Message = "status_message";
    public static final String Type_Control_Message = "control_message";
    public static final String Type_Code_VA = "code_va";
    public static final String Type_Chat_Message = "chat_message";
    @Expose
    //@SerializedName("code")
    public String code;
    @Expose
    public String status;
    @Expose
    public String type; // "code_va", "status_message", "control_message"

    public CompanionMessage() {
    }

    public CompanionMessage(String code) {
        this.code = code;
        this.type = "code_va";
    }

    public CompanionMessage(String type, String status) {
        this.type = type;
        this.status = status;
    }

    public static CompanionMessage chatMessage(String message) {
        CompanionMessage m = new CompanionMessage();
        m.code = message;
        m.type = Type_Chat_Message;
        m.status = "ok";
        return m;
    }
}
