package insalyon.bde.sia.adhesion.scannette.data;

import com.google.gson.GsonBuilder;
import com.tinder.scarlet.Scarlet;
import com.tinder.scarlet.WebSocket;
import com.tinder.scarlet.messageadapter.gson.GsonMessageAdapter;
import com.tinder.scarlet.streamadapter.rxjava2.RxJava2StreamAdapterFactory;
import com.tinder.scarlet.websocket.okhttp.OkHttpClientUtils;
import com.tinder.scarlet.ws.Receive;
import com.tinder.scarlet.ws.Send;

import io.reactivex.Flowable;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

public interface CompanionService {
    @Send
    void sendMessage(CompanionMessage message);

    @Receive
    Flowable<WebSocket.Event> observeWebSocketEvent();

    @Receive
    Flowable<CompanionMessage> observeMessages();

    class ScarlettBuilder {
        public static CompanionService build(String url) {
            return new Scarlet.Builder()
                    .webSocketFactory(OkHttpClientUtils.newWebSocketFactory(new OkHttpClient.Builder()
                            .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                            .build(), url)
                    ).addMessageAdapterFactory(new GsonMessageAdapter.Factory(new GsonBuilder().create()))
                    .addStreamAdapterFactory(new RxJava2StreamAdapterFactory())
                    .build().create(CompanionService.class);
        }
    }
}
