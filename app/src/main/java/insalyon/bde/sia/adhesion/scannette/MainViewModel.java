package insalyon.bde.sia.adhesion.scannette;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.LiveDataReactiveStreams;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.zxing.Result;

import insalyon.bde.sia.adhesion.scannette.data.CompanionMessage;
import insalyon.bde.sia.adhesion.scannette.data.CompanionService;

public class MainViewModel extends ViewModel {
    public final MutableLiveData<Result> scanResult = new MutableLiveData<>();
    public CompanionService cs;
    public LiveData<CompanionMessage> wsMessages;

    public MainViewModel() {
        Log.d("jean", "viewmodel constrcted");
        cs = CompanionService.ScarlettBuilder.build("http://192.168.1.2:8002/chat/");
        wsMessages = LiveDataReactiveStreams.fromPublisher(cs.observeMessages());
    }
}
