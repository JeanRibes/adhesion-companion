package insalyon.bde.sia.adhesion.scannette.ui.home;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import insalyon.bde.sia.adhesion.scannette.MainViewModel;
import insalyon.bde.sia.adhesion.scannette.R;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private MainViewModel viewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        final TextView textView = root.findViewById(R.id.text_home);
        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });

        viewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        viewModel.wsMessages.observe(getViewLifecycleOwner(), companionMessage -> {
            textView.setText(companionMessage.code);
            Log.i("jean", "chat message" + companionMessage.code);
        });
        return root;
    }
}