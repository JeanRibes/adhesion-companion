package insalyon.bde.sia.adhesion.scannette;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScannerFragment extends Fragment implements ZXingScannerView.ResultHandler {
    private ZXingScannerView mScannerView;
    private MainViewModel viewModel;

    public ScannerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_scanner, container, false);
        mScannerView = new ZXingScannerView(getActivity());
        mScannerView.setAutoFocus(true);
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
        mScannerView.resumeCameraPreview(this);
        Log.i("adhesion", "scanner fragment createView");

        viewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        return mScannerView;
    }

    @Override
    public void handleResult(Result result) {
        Log.i("adhesion", "scanned: " + result.getText() + " (format: " + result.getBarcodeFormat());
        viewModel.scanResult.postValue(result);
    }
}